import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;


public class EquationVerfierTest {

  private EquationVerfier verfier;

  private static final Map<String, String> equationMap;
  static{
    Map<String, String> tmpMap = new HashMap<String, String>();
    tmpMap.put("(([34[34]])234)", "TRUE");
    tmpMap.put("(([3a[34]])234)", "FALSE");
    tmpMap.put("([6)", "FALSE");
    tmpMap.put("([([()])])", "TRUE");
    tmpMap.put("([3)]", "FALSE");
    tmpMap.put("(]", "FALSE");
    tmpMap.put("))", "FALSE");
    tmpMap.put("", "TRUE");
    equationMap = new HashMap<String, String>(tmpMap);
  }

  @Before
  public void setUp(){
    verfier = new EquationVerfier();
  }

  @Test
  public void testExamples(){
    try {
      for(Map.Entry entry : equationMap.entrySet())
      testWithStandardInput((String)entry.getKey(), (String)entry.getValue());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void testWithStandardInput(String input, String expected) throws IOException {
    ByteArrayInputStream in = new ByteArrayInputStream((input).getBytes());
    BufferedInputStream stream = new BufferedInputStream(in);
    System.setIn(in);
    String result = verfier.analyzeInputData(stream);
    assertEquals(expected, result);
    in.close();
  }




}