import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Stack;

public class EquationVerfier {

  private Stack<Character> bracketStack;

  public EquationVerfier(){
    bracketStack = new Stack<Character>();
  }

  public void analizeInput(){
      try {
        checkInputStream();
      } catch (IOException e) {
        e.printStackTrace();
      }

  }

  private void checkInputStream() throws IOException {
    BufferedInputStream stream = new BufferedInputStream(System.in);
    while(true) {
      if(stream.available() > 0) {
        System.out.println(analyzeInputData(stream));
      }else{
        try {
          Thread.sleep(50);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  String analyzeInputData(BufferedInputStream stream) throws IOException {
    int c;
    boolean result = true;
    while ((c = stream.read()) != -1 && result) {
      if(c == 10){
        break;
      }else {
        result = analizeChar((char) c);
      }
    }
    String resultString = (getResult() && result) ? "TRUE" : "FALSE";
    bracketStack.clear();
    return resultString;
  }

  private boolean analizeChar(char c){
    switch(c) {
      case '[':
        bracketStack.push(c);
        break;
      case '(':
        bracketStack.push(c);
        break;
      case ']':
        if(!bracketStack.empty() && bracketStack.peek() == '['){
          bracketStack.pop();
        }else{
          return false;
        }
        break;
      case ')':
        if(!bracketStack.empty() && bracketStack.peek() == '('){
          bracketStack.pop();
        }else{
          return false;
        }
        break;
      default:
        if(!Character.toString(c).matches("[0-9]")) {
          return false;
        }
    }
    return true;
  }

  public boolean getResult() {
    return bracketStack.empty();
  }

}
